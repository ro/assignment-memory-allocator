#define _DEFAULT_SOURCE

#include "mem.h"

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);
extern inline bool region_is_invalid(const struct region *r);

static void block_init(void *restrict addr, block_size block_sz, void *restrict next)
{
  *((struct block_header *)addr) = (struct block_header){
      .next = next,
      .capacity = capacity_from_size(block_sz),
      .is_free = true};
}

static void *map_pages(void const *addr, size_t length, int additional_flags)
{
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

static size_t pages_count(size_t mem) { return mem / (size_t)getpagesize() + ((mem % (size_t)getpagesize()) > 0); }
static size_t round_pages(size_t mem) { return (size_t)getpagesize() * pages_count(mem); }

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static void *block_after(struct block_header const *block) { return (void *)(block->contents + block->capacity.bytes); }

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query)
{
  if (!addr)
  {
    return REGION_INVALID;
  }

  size_t size = region_actual_size(query);
  void *map_addres = map_pages(addr, size, MAP_FIXED | MAP_FIXED_NOREPLACE);

  if (map_addres == MAP_FAILED)
  {
    return REGION_INVALID;
  }
  struct region region = {.addr = map_addres, .size = size, .extends = true};

  int retry_count = 0;
  for (; map_addres == MAP_FAILED && retry_count < MAX_RETRY_COUNT; retry_count++)
  {
    map_addres = map_pages(addr, size, 0);
    if (map_addres == MAP_FAILED)
    {
      return REGION_INVALID;
    }
    region.extends = false;
  }

  if (map_addres != NULL && map_addres != MAP_FAILED)
  {
    block_init(map_addres, (block_size){size}, NULL);
  }

  return region;
}

void *heap_init(size_t initial)
{
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
  {
    return NULL;
  }
  return region.addr;
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query)
{
  return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query)
{
  query = size_max(query, BLOCK_MIN_CAPACITY);
  if (!block_is_big_enough(query, block))
  {
    return false;
  }
  if (!block_splittable(block, query))
  {
    return false;
  }
  block_size block_sz = {.bytes = block->capacity.bytes - query};
  void *addr = block->contents + query;
  block_init(addr, block_sz, block->next);
  block->next = addr;
  block->capacity.bytes = query;
  return true;
}

/*  --- Слияние соседних свободных блоков --- */
static bool blocks_continuous(
    struct block_header const *fst,
    struct block_header const *snd)
{
  return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd)
{
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block)
{
  if (!block && block->next->next) // мало ли NULL окажется
  {
    return false;
  }
  bool merged = false;
  if (block->next && mergeable(block, block->next))
  {
    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;
    merged = true;
  }
  return merged;
}

/*  --- ... ecли размера кучи хватает --- */

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz)
{
  struct block_search_result result = {
      .type = 0,
      .block = NULL};

  if (!block)
  {
    result.type = BSR_CORRUPTED;
    result.block = block;
    return result;
  }

  while (block && block->next)
  {
    if (block->is_free && block_is_big_enough(sz, block))
    {
      result = (struct block_search_result){
          .type = BSR_FOUND_GOOD_BLOCK,
          .block = block};
      split_if_too_big(block, sz);
      result.block->is_free = false;
      return result;
    }
    block = block->next;
  }

  if (block->is_free && block_is_big_enough(sz, block))
  {
    result = (struct block_search_result){
        .type = BSR_FOUND_GOOD_BLOCK,
        .block = block};
    split_if_too_big(block, sz);
    result.block = block;
    result.block->is_free = false;
    return result;
  }

  result.type = BSR_REACHED_END_NOT_FOUND;
  result.block = block;
  return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block)
{
  const struct block_search_result res = find_good_or_last(block, query);

  if (!res.type)
  {
    split_if_too_big(block, query);
  }
  return res;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query)
{
  if (!last)
  {
    return NULL;
  }

  struct region region;

  if (!block_after(last))
  {
    return NULL;
  }

  region.addr = block_after(last);
  region = alloc_region(region.addr, query);

  if (region_is_invalid(&region))
  {
    return NULL;
  }

  last->next = region.addr;

  if (try_merge_with_next(last))
  {
    return last;
  }

  return last->next;
}

//*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start)
{
  size_t need_to_alloc = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result found = try_memalloc_existing(need_to_alloc, heap_start);

  if (found.type == BSR_REACHED_END_NOT_FOUND)
  {
    struct block_header *new_block = grow_heap(found.block, need_to_alloc);
    if (new_block == NULL)
    {
      // можно было бы придумать можно ли таким что-то сделать
      return NULL;
    }
    memalloc(need_to_alloc, heap_start);
  }
  else if (found.type == BSR_CORRUPTED)
  {
    heap_init(need_to_alloc);
    memalloc(need_to_alloc, heap_start);
  }
  split_if_too_big(found.block, need_to_alloc);
  found.block->is_free = false;
  return found.block;
}

void *_malloc(size_t query)
{
  struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
  if (addr) return addr->contents;
  return NULL; // ловким движеним рук избавился от else
}

static struct block_header *block_get_header(void *contents)
{
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void _free(void *mem)
{
  if (!mem)
    return;
  struct block_header *header = block_get_header(mem);
  header->is_free = true;
  while (try_merge_with_next(header))
    ;
}
