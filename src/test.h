#ifndef TEST_MEM_HOCTb
#define TEST_MEM_HOCTb

#include <stdbool.h>
#include <stdio.h>

#include "mem.h"
#include "util.h"
#include "mem_internals.h"

#define HEAP_SIZE 10000

bool test1(void *heap);
bool test2(void *heap);
bool test3(void *heap);
bool test4(void *heap);
bool test5(void *heap);
bool kill_em_all(void *heap);

#endif
