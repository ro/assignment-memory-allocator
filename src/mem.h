#ifndef BLOCK_MIN_CAPACITY
#define BLOCK_MIN_CAPACITY 24
#endif

#ifndef _MEM_H_
#define _MEM_H_

#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>

#include "mem_internals.h"
#include "util.h"

#define HEAP_START ((void *)0x04040000)
#define DEBUG_FIRST_BYTES 4
#define MAX_RETRY_COUNT 9999

struct block_search_result
{
    enum
    {
        BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
        BSR_CORRUPTED
    } type;
    struct block_header *block;
};

void *_malloc(size_t query);
void _free(void *mem);
void *heap_init(size_t initial_size);

void debug_struct_info(FILE *f, void const *address);
void debug_heap(FILE *f, void const *ptr);

// чтобы однострочные там не валялись
void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

#endif
