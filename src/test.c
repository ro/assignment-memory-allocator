#define _DEFAULT_SOURCE

#include "test.h"

#define V0          0
#define V1          128
#define V2          512
#define V3          1000
#define V4          50000
#define ULTRA_SIZE  300000

static struct block_header *header_of(void *contents) // не буду тянуть статики из mem.c, скопирую и преназову кратче
{
    return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

static void *mapp(void const *addr, size_t length)
{
    return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
}

static void seek_and_destroy(void *heap) // лучше, если куча в начале каждого из тестов в одинкаковом состоянии
{
    struct block_header *block = heap;
    while (block)
    {
        _free(block->contents);
        block = block->next;
    }
    printf("Подчистил за собой\n");
    debug_heap(stdout, heap);
}

bool test_1(void *heap)
{
    bool flag = false;
    printf("\n\n1: Аллокатор\n\n\n");
    debug_heap(stdout, heap);

    // Размещение объёмов V0 и V1
                          void *part_0 = _malloc(V0);
                          void *part_1 = _malloc(V1);
    printf("Пихаем %d и %d:\n", V0, V1);
    debug_heap(stdout, heap);

    if (part_0 != NULL && part_1 != NULL)
    {
    struct block_header *header0 = header_of(part_0);
    struct block_header *header1 = header_of(part_1);

        if (!header0->is_free && header0->capacity.bytes == BLOCK_MIN_CAPACITY)
        {

            if (!header1->is_free && header1->capacity.bytes == V1)
            {
                                        flag = true;
         printf("\n\n\t\t\tРезульат: ОТЛИЧНО!\n\n");
            }
            else
            {
                fprintf(stderr, "\nРезульат: не вышло аллоцировать %d.\n", V1);
            }
        }
        else
        {
            fprintf(stderr, "\nРезульат: не получилось обработать 0.\n");
        }
    }
    else
    {
        fprintf(stderr, "\nРезульат: аллоцирован NULL\n");
    }

                                      _free(part_1);
                                      _free(part_0);
    return flag;
}

bool test_2(void *heap)
{
    bool flag = false;
    printf("\n\n2: Освободим блок\n\n\n");
    printf("ДО аллокации:\n");
                           debug_heap(stdout, heap);

    printf("ПОСЛЕ аллокации:\n");
                         void *part_2 = _malloc(V2);
    struct block_header *header = header_of(part_2);
                           debug_heap(stdout, heap);

     printf("Освобожу тот кусок, который %d\n", V2);
                                      _free(part_2);

    printf("Псоле освобождения имеем:\n");
                           debug_heap(stdout, heap);

    if (header->is_free)
    {
                                        flag = true;
         printf("\n\n\t\t\tРезульат: ОТЛИЧНО!\n\n");
    }
    else
    {
        fprintf(stderr, "Эта память не по-настоящему свобоодна.\n");
    }
    return flag;
}

bool test_3(void *heap)
{
    bool flag = false;
    printf("\n\n3: Два свободных\n\n\n");

                         void *part_1 = _malloc(V2);
                         void *part_2 = _malloc(V2);
                         void *part_3 = _malloc(V2);
                         void *part_4 = _malloc(V2);

    printf("Разместил кучку куч:\n");
                           debug_heap(stdout, heap);

    struct block_header *block1 = header_of(part_1);
    struct block_header *block2 = header_of(part_2);
    struct block_header *block3 = header_of(part_3);
    struct block_header *block4 = header_of(part_4);

                                      _free(part_2);
                                      _free(part_3);
    printf("Освободил вторую и третью:\n");
                           debug_heap(stdout, heap);

                                      _free(part_4);
    printf("Освободил четвёртую:\n");
                           debug_heap(stderr, heap);
    if (block4->is_free && block2->is_free && block3->is_free)
    {
        flag = true;
    }
                                      _free(part_1);
    printf("Освободил первую:\n");
                           debug_heap(stdout, heap);

    if (block1->is_free)
    {
        flag = flag && true;
    }

    flag ? printf("\n\n\t\t\tРезульат: ОТЛИЧНО!\n\n") : fprintf(stderr, "\nРезульат не сросся\n");
    return flag;
}

bool test_4(void *heap)
{
    bool flag = false;

    printf("\n\n4: Масштабируемость кучи\n\n\n");

    // Размещу V3 и V4
                        void *part_1 = _malloc(V3);
                        void *part_2 = _malloc(V4);
    printf("Попытка аллоцировать %d, а потом ещё %d:\n", V3, V4);
    debug_heap(stdout, heap);

    struct block_header *block1 = header_of(part_1);
    struct block_header *block2 = header_of(part_2);

    if (block1->next == block2)
    {
                                        flag = true;
         printf("\n\n\t\t\tРезульат: ОТЛИЧНО!\n\n");
    }
    else
    {
        fprintf(stderr, "Не вышло.\n");
    }

                                      _free(part_2);
                                      _free(part_1);
    return flag;
}

bool test_5(void *heap)
{
    bool flag = false;

    printf("\n\n5: В старой области паяти мало места для двоих\n\n");

                         void *part_1 = _malloc(V1);
    struct block_header *block1 = header_of(part_1);

    mapp(block1->contents + block1->capacity.bytes, REGION_MIN_SIZE);

                           debug_heap(stdout, heap);

    // Попытка аллоцировать ULTRA_SIZE
                 void *part_2 = _malloc(ULTRA_SIZE);
    struct block_header *block2 = header_of(part_2);

   printf("Попытка аллоцировать %d:\n", ULTRA_SIZE);
   
                           debug_heap(stdout, heap);

    if (block1->contents + block1->capacity.bytes == (void *)block2 && block1 && block2)
    {
                                        flag = true;
         printf("\n\n\t\t\tРезульат: ОТЛИЧНО!\n\n");
    }
    else
    {
        fprintf(stderr, "Куда попали? Неизвестно.\n");
    }

    return flag;
}

bool    kill_em_all(void *heap)
    {   //тут идеальная ширина! 
        bool t1 = test_1(heap);
        seek_and_destroy(heap);
        bool t2 = test_2(heap);
        seek_and_destroy(heap);
        bool t3 = test_3(heap);
        seek_and_destroy(heap);
        bool t4 = test_4(heap);
        seek_and_destroy(heap);
        bool t5 = test_5(heap);
        t1 = t1 &&  t2  &&  t3;
        return  t1 && t4 && t5;
    }   //воистину великолепно)
